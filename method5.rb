def who_am_i(name)
  hello = "こんにちは、"
  puts hello + name
end

who_am_i("たけみつ")

puts hello + "たけみつさん。" #エラーが出る。

## 変数のスコープ

#　メソッドの中で定義された変数はメソッドの中でしか使用できない。
