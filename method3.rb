def who_am_i(name, age = 0)
  puts "こんにちは、私は#{age}歳の#{name}です。"
end



#複数の引数を受け取ることもできる。順番が重要。
who_am_i(3, "山本") # nameに年齢が、ageに名前が入ってしまう。

who_am_i("山本")


who_am_i("中村",18)
who_am_i("アレックス",32)
