def who_am_i(name) #引数を取るメソッド
  puts "私の名前は#{name}です。"
end

#メソッドの定義方法
# def メソッド名(仮引数)
#   処理内容
# end

who_am_i("たけみつ")
who_am_i("けんじ")
who_am_i("まろ")
