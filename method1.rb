def hello()
  puts "Hello, world!"
end

#メソッドの定義方法
# def メソッド名(仮引数)
#   処理内容
# end

hello()   #メソッドの呼び出し
hello()
hello()
